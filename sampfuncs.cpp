#include "sampfuncs.h"

SF *SF::self = nullptr;

SF::SF() {
	lib = LoadLibraryA( "SAMPFUNCS.asi" );
	_SF = new origSF();

	_Log = std::bind(
		(void ( * )( origSF *, const char *, ... ))GetProcAddress( lib, "?Log@SAMPFUNCS@@QAAXPBDZZ" ), _SF,
		std::placeholders::_1 );
	_Log_str = std::bind(
		(void ( * )( origSF *, const std::string & ))GetProcAddress(
			lib, "?Log@SAMPFUNCS@@QAEXABV?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@std@@@Z" ),
		_SF, std::placeholders::_1 );
	_LogConsole = std::bind(
		(void ( * )( origSF *, const char *, ... ))GetProcAddress( lib, "?LogConsole@SAMPFUNCS@@QAAXPBDZZ" ),
		_SF, std::placeholders::_1 );
	_LogFile = std::bind(
		(void ( * )( origSF *, const char *, ... ))GetProcAddress( lib, "?LogFile@SAMPFUNCS@@QAAXPBDZZ" ), _SF,
		std::placeholders::_1 );
	execConsoleCommand = std::bind( (void ( * )( origSF *, std::string ))GetProcAddress(
										lib, "?execConsoleCommand@SAMPFUNCS@@QAEXV?$basic_string@DU?$char_"
											 "traits@D@std@@V?$allocator@D@2@@std@@@Z" ),
									_SF, std::placeholders::_1 );
	getAPIVersion = std::bind(
		(unsigned ( * )( origSF * ))GetProcAddress( lib, "?getAPIVersion@SAMPFUNCS@@QAEIXZ" ), _SF );
	getConsoleCommands = std::bind( (std::vector<stCommandInfo>( * )( origSF * ))GetProcAddress(
										lib, "?getConsoleCommands@SAMPFUNCS@@QAE?AV?$vector@UstCommandInfo@@"
											 "V?$allocator@UstCommandInfo@@@std@@@std@@XZ" ),
									_SF );
	getSFVersion =
		std::bind( (unsigned ( * )( origSF * ))GetProcAddress( lib, "?getSFVersion@SAMPFUNCS@@QAEIXZ" ), _SF );
	isConsoleOpened =
		std::bind( (bool ( * )( origSF * ))GetProcAddress( lib, "?isConsoleOpened@SAMPFUNCS@@QAE_NXZ" ), _SF );
	registerConsoleCommand = std::bind( (void ( * )( origSF *, std::string, CommandProc ))GetProcAddress(
											lib, "?registerConsoleCommand@SAMPFUNCS@@QAEXV?$basic_string@DU?$"
												 "char_traits@D@std@@V?$allocator@D@2@@std@@P6GX0@Z@Z" ),
										_SF, std::placeholders::_1, std::placeholders::_2 );
	setConsoleCommandDescription =
		std::bind( (void ( * )( origSF *, std::string, std::string ))GetProcAddress(
					   lib, "?setConsoleCommandDescription@SAMPFUNCS@@QAEXV?$basic_string@DU?$char_traits@D@"
							"std@@V?$allocator@D@2@@std@@0@Z" ),
				   _SF, std::placeholders::_1, std::placeholders::_2 );
	unregisterConsoleCommand = std::bind( (void ( * )( origSF *, std::string ))GetProcAddress(
											  lib, "?unregisterConsoleCommand@SAMPFUNCS@@QAEXV?$basic_string@"
												   "DU?$char_traits@D@std@@V?$allocator@D@2@@std@@@Z" ),
										  _SF, std::placeholders::_1 );
}

SF::~SF() { delete _SF; }


SF *SF::Instance() {
	if ( !self ) self = new SF();
	return self;
}

void SF::DeleteInstance() {
	if ( !self ) return;
	delete self;
	self = nullptr;
}

bool SF::isLoaded() { return lib != 0 && lib != reinterpret_cast<HMODULE>(-1); }

void SF::Log( const char *Text, ... ) {
	if ( !isLoaded() ) return;
	va_list ap;
	va_start( ap, Text );
	vsprintf( format, Text, ap );
	va_end( ap );
	_Log( format );
}

void SF::Log( const std::string &Text ) {
	if ( !isLoaded() ) return;
	_Log_str( Text );
}

void SF::LogConsole( const char *Text, ... ) {
	if ( !isLoaded() ) return;
	va_list ap;
	va_start( ap, Text );
	vsprintf( format, Text, ap );
	va_end( ap );
	_LogConsole( format );
}

void SF::LogFile( const char *Text, ... ) {
	if ( !isLoaded() ) return;
	va_list ap;
	va_start( ap, Text );
	vsprintf( format, Text, ap );
	va_end( ap );
	_LogFile( format );
}
