#ifndef SF_H
#define SF_H

#include <vector>
#include <map>
#include <string>
#include <windows.h>
#include <functional>
#include <regex>

typedef void(__stdcall *CommandProc) (std::string params);
struct stCommandInfo
{
	enum CommandType { NOPE, SCRIPT, PLUGIN };

	stCommandInfo(std::string n, CommandType t, void* o){
		name = n;
		type = t;
		owner = o;
	}

	std::string					name;
	CommandType					type;
	void                        *owner;
};

/// Болванка для скармливания некоторым функциям SF.
class origSF
{
public:
	void *pPlugin;
};

/// Класс для взаимодействия с консолью SF.
class SF
{
	SF();
	~SF();
	static SF *self;
public:
	static SF *Instance();
	static void DeleteInstance();
	/// \return \b bool -> проверка, что SF загружен в игру.
	bool isLoaded();

	/**
	 * \brief Логирование в консоль и в файл.
	 * \param[in] format Формат строки.
	 * \param[in] args Аргументы для форматирования строки.
	 */
	void Log(const char*, ...);
	/**
	 * \brief Логирование в консоль и в файл.
	 * \param[in] string Строка.
	 */
	void Log(const std::string &);
	/**
	 * \brief Логирование только консоль.
	 * \param[in] format Формат строки.
	 * \param[in] args Аргументы для форматирования строки.
	 */
	void LogConsole(const char*, ...);
	/**
	 * \brief Логирование только в файл
	 * \param[in] format Формат строки.
	 * \param[in] args Аргументы для форматирования строки.
	 */
	void LogFile(const char*, ...);

	/// \return \b unsigned -> Возвращает версию SFAPI.
	std::function<unsigned()> getAPIVersion;
	/// \return \b unsigned -> Возвращает версию SF.
	std::function<unsigned()> getSFVersion;

	/// \param[in] command Выполняет команду. Аргументы передаются вместе с командой.
	std::function<void(std::string)> execConsoleCommand;
	/// \return Возвращает вектор команд.
	std::function<std::vector<stCommandInfo>()> getConsoleCommands;
	/**
	 * \brief Регистрация новой команды в консоле SF.
	 * \param[in] command Команда.
	 * \param[in] proc Функция, которая будет вызываться при вводе команды.
	 */
	std::function<void(std::string, CommandProc)> registerConsoleCommand;
	/**
	 * \brief Задает описание команде.
	 * \param[in] command Команда.
	 * \param [in] description Описание.
	 */
	std::function<void(std::string, std::string)> setConsoleCommandDescription;
	/**
	 * \brief Удаление команды из консоли SF.
	 * \param[in] command Команда.
	 */
	std::function<void(std::string)> unregisterConsoleCommand;

	/// \return \b bool -> Проверяет, открыта ли консоль.
	std::function<bool()> isConsoleOpened;

private:
	HMODULE lib = 0;
	origSF *_SF;
	char format[102400];

	std::function<void(const char*)> _Log;
	std::function<void(const std::string &)> _Log_str;
	std::function<void(const char*)> _LogConsole;
	std::function<void(const char*)> _LogFile;
};

#endif // SF_H
